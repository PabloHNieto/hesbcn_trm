# Standalone HESBCN/PMCE

By running 

```
source("hesbcn_example.R")
```

you will execute hesbcn plus some extra functions to obtain the
transition matrix.
Addittionaly some tests found in `test.hesbcn.get.right.transitions.R` will also be run.

The functionality is implemented in `hesbcn.R`. This code is part of a larger project and we have copied here just the bare minimum. There is a wrapper to run `hesbcn`, a modified verion of `import.hesbcn` plus function to compute all available genotypes given the dag and the relationships and to compute the transitions matrix.

We hope you find this helpful.


Pablo Herrera Nieto (<pablo.herrera@uam.es>), Ramon Diaz-Uriarte
(<r.diaz@uam.es>).


## License
The code is licensed under the GPLv3 license.
